<?php
namespace DrkService\DrkOrderform\Domain\Repository;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2023 André Gyöngyösi <a.gyoengyyoesi@drkserivce.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use DRK\DrkGeneral\Domain\Repository\AbstractDrkRepository;
use DRK\DrkGeneral\Utilities\Utility;
use TYPO3\CMS\Core\Cache\Exception\NoSuchCacheException;

/**
 * The repository for Orders
 */
class OrderFormRepository extends AbstractDrkRepository
{

    /**
     *
     * sendOrderData
     *
     * @param array $aSendObject
     * @return bool
     */
    public function sendOrderData(array $aSendObject = []): bool
    {
        if (empty($aSendObject)) {
            $this->error = ['Error' => 'No object to send!'];
            return false;
        }

        try {
            $aResult = $this->executeJsonClientAction('sendOrderData', $aSendObject);

            // Result is NULL, if no errors occur
            if (empty($aResult)) {
                return true;
            } else {
                return $aResult;
            }
        } catch (\Exception $exception) {
            $this->error = ['Fehler' => $exception->getMessage()];
            return false;
        }
    }

    public function getOrganisationidByZipcode (string $zipCode): int|null
    {

        if (trim($zipCode) != '' AND is_numeric($zipCode) AND strlen($zipCode) == 5) {

            $request = [
                $this->settings['apiKey'],
                $zipCode
            ];

            try {
                $response = $this->executeJsonClientAction('getOrgidByZip', $request);

                // Result is NULL, if no errors occur
                if (!empty($response) &&
                    array_key_exists('org_id', $response)
                ) {
                    return $response['org_id'];
                } else {
                    return null;
                }
            } catch (\Exception $exception) {
                $this->error = ['Fehler' => $exception->getMessage()];
                return null;
            }

        }
        return null;
    }

    /**
     * @param string $sortBy
     * @param string $orderBy
     * @return array
     * @throws NoSuchCacheException
     */
    public function findOfferList(string $sortBy = 'description', string $orderBy = 'asc'): array
    {

        $cacheIdentifier = sha1(json_encode(['drk_orderform|findOfferList', $sortBy, $orderBy]));
        $result = $this->getCache()->get($cacheIdentifier);

        if (($result !== null) && ($result !== false)) {
            return $result;
        }

        $client = $this->getDldbClient();
        if ($client) {
            $offers = $offerLinks = Utility::convertObjectToArray(
                $client->getOfferDescriptionList($sortBy, $orderBy)
            );
        }

        if (empty($offers)) {
            return [];
        }
        $this->getCache()->set($cacheIdentifier, $offers, [], $this->heavy_cache);

        return $offers;
    }

    /**
     * ItemsProcFunc for SupplyFinder->settings.flexforms.preselection
     *
     * @param array $parameters
     * @return array
     * @throws NoSuchCacheException
     */
    public function addOfferListItems(array $parameters): array
    {
        $this->initSettingsForListItems($parameters);
        foreach ($this->findOfferList() as $offer) {
            $parameters['items'][] = [
                $offer['orgOfferDescription'],
                $offer['orgOfferId'],
                '',
            ];
        }

        return $parameters;
    }
}
