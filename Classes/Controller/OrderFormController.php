<?php
namespace DrkService\DrkOrderform\Controller;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2023 André Gyöngyösi <a.gyoengyyoesi@drkserivce.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use DRK\DrkGeneral\Controller\AbstractDrkController;
use DRK\DrkGeneral\Utilities\Utility;
use DrkService\DrkOrderform\Domain\Repository\OrderFormRepository;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Core\Routing\PageArguments;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Page\AssetCollector;
use TYPO3\CMS\Form\Domain\Model\Renderable\RenderableInterface;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;

/**
 * OrderController
 */
class OrderFormController extends AbstractDrkController
{

    /**
     * orderFormRepository
     *
     * @var \DrkService\DrkOrderform\Domain\Repository\OrderFormRepository
     */
    protected ?OrderFormRepository $orderFormRepository = NULL;

    /**
     * @var array
     */
    protected $mandatoryFormFields = [
        'person' => ['vorname', 'name'],
    ];

    /**
     * Order subject
     *
     * @var array
     */
    protected array $subjectSelectionArray = [];

    /**
     * Error message
     *
     * @var array
     */
    public $error = [];

    /**
     * @var array[]|string[]
     */
    private array $urlArguments;

    /**
     * @param AssetCollector $assetCollector
     */
    public function __construct(
        private readonly AssetCollector $assetCollector
    ) {
    }

    public function injectOrderFormRepository(OrderFormRepository $repository): void
    {
        $this->orderFormRepository = $repository;
    }

    /**
     * Init
     *
     * @return void
     */
    protected function initializeAction(): void
    {
        parent::initializeAction();

        /** @var \TYPO3\CMS\Core\Http\ServerRequest $typo3Request */
        $typo3Request = $GLOBALS['TYPO3_REQUEST'] ?? null;

        if ($typo3Request instanceof ServerRequestInterface) {
            $queryArguments = $typo3Request->getAttribute('routing');
            if ($queryArguments instanceof PageArguments) {
                $this->urlArguments = $queryArguments->getArguments();
            }
        }

        $this->assetCollector->addStyleSheet(
            'Orderform',
            'EXT:drk_orderform/Resources/Public/Css/styles.css'
        );

        $this->assetCollector->addJavaScript(
            'Orderform',
            'EXT:drk_orderform/Resources/Public/Scripts/tx_drkorderform.js',
            [],
            ['priority' => false] // lädt in footer

        );

        // split subscription from flexform
        $this->splitSubject();

    }

    /**
     * @param RenderableInterface $renderable
     * @return void
     */
    public function initializeFormElement(RenderableInterface $renderable): void
    {
        switch ($renderable->getUniqueIdentifier()) {

            case 'drk_orderform-p_prefix':
                $renderable->setProperty('options', Utility::$prefixArray);
                break;

            case 'drk_orderform-p_title':
                $renderable->setProperty('options', Utility::$titleArray);
                break;
        }
    }


    /**
     * action show
     *
     * @return void
     */
    public function showOrderFormAction(): ResponseInterface
    {

        //create privacy_url
        if (!isset($this->settings['privacyUrl']) || empty($this->settings['privacyUrl']) || !$this->validateUrl($this->settings['privacyUrl'])) {
            $privacyHint = "";
        } else {

            $content = "<b>Datenschutz-Hinweise</b>";
            $instructions = [
                'parameter' => $this->settings['privacyUrl'],
                'ATagParams' => "",
                'forceAbsoluteUrl' => false,
            ];

            $contentObject = GeneralUtility::makeInstance(ContentObjectRenderer::class);

            $privacyHint = 'Mit dem Absenden des Formulars bestätige ich, dass ich die '.$contentObject->typoLink($content, $instructions).' zur Kenntnis genommen habe.';
        }

        //build headline
        $header_offset = "Bestellformular";
        $contentObject = $this->configurationManager->getContentObject()->data;

        if ( isset($contentObject['header']) && !empty ($contentObject['header']) )
        {
            $header_offset = $contentObject['header'];

        }

        if (!empty($this->settings['preselection']) && $this->settings['preselection'] > 0) {
            foreach ($this->orderFormRepository->findOfferList() as $offer) {
                if ($offer['orgOfferId'] == $this->settings['preselection']) {
                    $headline = $header_offset . " - " . $offer["orgOfferDescription"];
                    break;
                }
            }
        } else {
            $headline = $header_offset;
        }

        /**
         * overrideConfiguration Array
         */

        //set variable fields
        $overrideConfiguration = array(
            'renderables' =>
                [
                    0 =>
                        [
                            'renderables' =>
                            [
                                0 =>
                                    [
                                        'renderables' =>
                                            [
                                                0 =>
                                                    [
                                                        'label' => $headline,
                                                    ],
                                                10 =>
                                                    [
                                                        'renderables' =>
                                                            [
                                                                0 =>
                                                                    [
                                                                        'properties' =>
                                                                            [
                                                                                'options' => $this->subjectSelectionArray
                                                                            ],
                                                                    ],
                                                            ],
                                                    ],
                                                12 =>
                                                    [
                                                        'renderables' =>
                                                            [
                                                                0 =>
                                                                    [
                                                                        'properties' =>
                                                                            [
                                                                                'text' => $privacyHint
                                                                            ],
                                                                    ],
                                                            ],
                                                    ]
                                            ],
                                    ],
                            ],
                        ],
                ],
        );

        /**
         * mandatory fields
        */
        $_required = [
            'properties' => [
                'fluidAdditionalAttributes' => [
                    'required' => "required",
                ],
                'validationErrorMessages' => [
                    0 => [
                        'code'=>1221560910,
                        'message' => 'Das ist ein Pflichtfeld.',
                    ],
                ],
            ],
            'validators' => [
                0 => [
                    'identifier' => 'NotEmpty',
                ]
            ]
        ];

        if ($this->settings['requireAddressField'] == 1) {
            $overrideConfiguration['renderables'][0]['renderables'][0]['renderables'][5]['renderables'][0] = $_required;
            $overrideConfiguration['renderables'][0]['renderables'][0]['renderables'][6]['renderables'][0] = $_required;
            $overrideConfiguration['renderables'][0]['renderables'][0]['renderables'][7]['renderables'][0] = $_required;
        }

        if ($this->settings['requirePhoneField'] == 1) {
            $overrideConfiguration['renderables'][0]['renderables'][0]['renderables'][8]['renderables'][0] = $_required;
        }

        if ($this->settings['requireMailField'] == 1) {
            $overrideConfiguration['renderables'][0]['renderables'][0]['renderables'][9]['renderables'][0] = $_required;
        }

        if ($this->settings['setOrganisationByZip'] == 1) {
            $overrideConfiguration['renderables'][0]['renderables'][0]['renderables'][6]['renderables'][0] = $_required;
        }

        /**
         * hide fields
         */

        if (!$this->settings['showAddressField']) {
            $overrideConfiguration['renderables'][0]['renderables'][0]['renderables'][6] = "__UNSET";
            $overrideConfiguration['renderables'][0]['renderables'][0]['renderables'][7] = "__UNSET";
            $overrideConfiguration['renderables'][0]['renderables'][0]['renderables'][8] = "__UNSET";
        }

        if (!$this->settings['showPhoneField']) {
            $overrideConfiguration['renderables'][0]['renderables'][0]['renderables'][9] = "__UNSET";
        }

        if (!$this->settings['showMailField']) {
            $overrideConfiguration['renderables'][0]['renderables'][0]['renderables'][10] = "__UNSET";
        }

        // get offerid if given
        if (!empty($this->urlArguments)) {
            foreach ($this->urlArguments as $urlArgumentKey=>$urlArgumentValue) {
                if ($urlArgumentKey === "offer_id" and is_numeric($urlArgumentValue)) {
                    $overrideConfiguration['renderables'][0]['renderables'][1] =
                        [
                            'type' => 'Hidden',
                            'identifier' => 'offer_id',
                            'defaultValue' => intval($urlArgumentValue),
                            'label' => 'offer_id',
                        ];
                    break;
                }
            }
        }

        $this->view->assign('overrideConfiguration', $overrideConfiguration);

        return $this->htmlResponse();
    }

    /**
     * splitSubject
     *
     * @return void
     */
    private function splitSubject(): void
    {
        if (empty($this->settings['subject'])) {
            $this->subjectSelectionArray = ['Informationsmaterial zusenden'];
        } else {
            $this->subjectSelectionArray = preg_split('/\\r\\n|\\r|\\n/', $this->settings['subject']);
        }
    }

    /**
     * action send
     *
     * @return void|ResponseInterface
     */
    public function sendAction()
    {

        $aFormData = [];
        $this->view->assign('sending_ok', false);
        $this->view->assign('debug', $this->settings['debug']);
        $aArguments = $this->request->getArguments()['drk_orderform'];

        $errors = false;

        // Check for existing api key
        if ( !isset($this->settings['apiKey']) || empty ($this->settings['apiKey']) )
        {
            $this->error = array('Fehler' => 'Formular kann nicht abgesendet werden. API-Schlüssel fehlt!');
            $errors = true;
        }

        if (!empty($aArguments)) {
            array_key_exists('p_prefix', $aArguments) ? $aFormData['anrede'] = (int)$aArguments['p_prefix'] : $aFormData['anrede'] = 1;
            array_key_exists('p_title', $aArguments) ? $aFormData['titel'] = (int)$aArguments['p_title'] : $aFormData['titel'] = 0;
            array_key_exists('p_lastanme', $aArguments) ? $aFormData['name'] = $aArguments['p_lastanme'] : $aFormData['name'] = "";
            array_key_exists('p_firstname', $aArguments) ? $aFormData['vorname'] = $aArguments['p_firstname'] : $aFormData['vorname'] = "";
            array_key_exists('p_street', $aArguments) ? $aFormData['strasse'] = $aArguments['p_street'] : $aFormData['strasse'] = "";
            array_key_exists('p_city', $aArguments) ? $aFormData['ort'] = $aArguments['p_city'] : $aFormData['ort'] = "";
            array_key_exists('p_zip', $aArguments) ? $aFormData['plz'] = $aArguments['p_zip'] : $aFormData['plz'] = "";
            array_key_exists('p_phone', $aArguments) ? $aFormData['telefon'] = $aArguments['p_phone'] : $aFormData['telefon'] = "";
            array_key_exists('p_mail', $aArguments) ? $aFormData['email'] = $aArguments['p_mail'] : $aFormData['email'] = "";
            array_key_exists('message', $aArguments) ? $aFormData['nachricht'] = $aArguments['message'] : $aFormData['nachricht'] = "";
            array_key_exists('offer_id', $aArguments) ? $aFormData['offer_id'] = $aArguments['offer_id'] : $aFormData['offer_id'] = 0;


            // get subject from form
            if (array_key_exists('subject', $aArguments) && is_array($aArguments['subject'])) {
                $lastKeyInArray = array_key_last($aArguments['subject']);
                $aFormData['betreff'] = '';
                foreach ($aArguments['subject'] as $subjectKey) {
                    if (is_numeric($subjectKey) && array_key_exists($subjectKey, $this->subjectSelectionArray)) {
                        $aFormData['betreff'] .= $this->subjectSelectionArray[$subjectKey];
                        if ($lastKeyInArray != $subjectKey) {
                            $aFormData['betreff'] .= ',';
                        }
                    }
                }
            } else {
                $aFormData['betreff'] = "Sonstiges";
            }

            //set OfferTypId
            if ($this->settings['preselection'] && is_numeric($this->settings['preselection'])) {
                $aFormData['servicetyp'] = $this->settings['preselection'];
            } else {
                $aFormData['servicetyp'] = 0;
            }

            //set target offer id by zip
            if ($this->settings['setOrganisationByZip']) {
                $aFormData['targetorg'] = $this->orderFormRepository->getOrganisationidByZipcode($aFormData['plz']);
            }
        }

        //check if we have an empty form
        $sCheck = $aArguments['p_lastanme'].$aArguments['p_firstname'].$aArguments['p_mail'];
        if (empty($sCheck)) {
            $this->error = array('Fehler' => 'Bitte kein leeres Formular absenden!');
            $errors = true;
        }

        //check honeypot field
        if (!$this->isHoneypotFilled($aArguments['p_birthname']))
        {
            $errors = true;
        }

        $mandatoryFieldsFilled = $this->checkMandatoryFormFields($this->mandatoryFormFields['person'], $aFormData);

        if (!$errors && $mandatoryFieldsFilled) {
            $request = [
                $this->settings['apiKey'],
                $aFormData
            ];

            // sending Data are successful

            if ($this->orderFormRepository->sendOrderData($request) && !$this->orderFormRepository->getErrors()) {
                $this->view->assign('sending_ok', true);

                // if successPageId is set, the redirect to this page
                if ($this->settings['successPageId'] && !$this->settings['debug']) {
                    $uriBuilder = $this->uriBuilder;
                    $uriBuilder->reset();
                    $uriBuilder->setTargetPageUid($this->settings['successPageId']);
                    $uriBuilder->setCreateAbsoluteUri(true);
                    $uri = $uriBuilder->build();
                    return $this->responseFactory->createResponse(
                        303,
                        ''
                    )->withAddedHeader('Location', $uri);
                }
                // else go on and render the template
                unset($request);

                $aClientData = $aFormData;
                $aClientData['anrede'] = array_key_exists($aFormData['anrede'], Utility::$prefixArray) ? Utility::$prefixArray[$aFormData['anrede']] : "Herr";
                $aClientData['titel'] = array_key_exists($aFormData['titel'], Utility::$titleArray) ? Utility::$titleArray[$aFormData['titel']] : "";

                $this->view->assign('aClientData', $aClientData);
            }
        }

        // show debug
        if ($this->settings['debug']) {
            $this->view->assign('debug', $aArguments);
        }

        // if Json-Errors, then show it now
        if ($sError = $this->orderFormRepository->getErrors()) {
            $this->error = array_merge($this->error, $sError);
            $errors = true;
        }

        if ($errors) {
            $this->view->assign('error', true);
            $this->view->assign('errorMessages', $this->error);
        } else {
            $this->view->assign('error', false);
        }

        return $this->htmlResponse();
    }
}
