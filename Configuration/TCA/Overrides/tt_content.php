<?php

// Orderform Plugin
$pluginSignature = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'drk_orderform',
    'Orderform',
    'LLL:EXT:drk_orderform/Resources/Private/Language/locallang_be.xlf:tt_content.orderform_plugin.title',
    'EXT:drk_orderform/Resources/Public/Icons/drk-logo-icon.svg',
    'DRK Bestellformular'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    '*',
    // FlexForm configuration schema file
    'FILE:EXT:drk_orderform/Configuration/FlexForms/orderform.xml',
    // ctype
    'drkorderform_orderform'
);

$GLOBALS['TCA']['tt_content']['types'][$pluginSignature] = array_replace_recursive(
    $GLOBALS['TCA']['tt_content']['types'][$pluginSignature],
    [
        'showitem' => '
            --div--;General,
            --palette--;General;general,
            --palette--;Headers;headers,
            --div--;Einstellungen,
            pi_flexform'
    ]
);

