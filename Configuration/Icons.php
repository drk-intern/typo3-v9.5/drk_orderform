<?php

return [
    'drk-logo-icon' => [
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        'source' => 'EXT:drk_orderform/Resources/Public/Icons/drk-logo-icon.svg',
    ],
];
