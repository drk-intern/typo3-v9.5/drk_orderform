<?php

namespace DrkService\DrkOrderform\Tests\Unit\Controller;

use DrkService\DrkOrderform\Controller\OrderController;
use DrkService\DrkOrderform\Domain\Model\Order;
use TYPO3\CMS\Core\Tests\UnitTestCase;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 Hans-Jürg Ungeheuer <ungeheuh@drk.de>, Deutsches Rotes Kreuz
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class DrkService\DrkOrderform\Controller\OrderController.
 *
 * @author Hans-Jürg Ungeheuer <ungeheuh@drk.de>
 */
class OrderControllerTest extends UnitTestCase
{

    /**
     * @var OrderController
     */
    protected $subject = null;

    public function setUp()
    {
        $this->subject = $this->getMock(
            'DrkService\\DrkOrderform\\Controller\\OrderController',
            array('redirect', 'forward', 'addFlashMessage'),
            array(),
            '',
            false
        );
    }

    public function tearDown()
    {
        unset($this->subject);
    }

    /**
     * @test
     */
    public function showActionAssignsTheGivenOrderToView()
    {
        $order = new Order();

        $view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
        $this->inject($this->subject, 'view', $view);
        $view->expects($this->once())->method('assign')->with('order', $order);

        $this->subject->showAction($order);
    }
}
