<?php
if (!defined('TYPO3')) {
    die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'drk_orderform',
    'Orderform',
    [
        \DrkService\DrkOrderform\Controller\OrderFormController::class => 'showOrderForm, send',

    ],
    // non-cacheable actions
    [
        \DrkService\DrkOrderform\Controller\OrderFormController::class => 'send',
    ],
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['ext/form']['initializeFormElement'][1570192584] = DrkService\DrkOrderform\Controller\OrderFormController::class;



